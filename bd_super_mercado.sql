CREATE TABLE supermercado (
    id             INTEGER      PRIMARY KEY AUTO_INCREMENT,
    nombre         VARCHAR (50) NOT NULL,
    direccion      VARCHAR (50) NOT NULL
);

CREATE TABLE propietario (
    id       INTEGER      PRIMARY KEY AUTO_INCREMENT,
    nombre   VARCHAR (50) NOT NULL,
    apellido VARCHAR (50) NOT NULL,
    super_id INTEGER      NOT NULL,
    FOREIGN KEY (
        super_id
    )
    REFERENCES supermercado (id) 
);

CREATE TABLE horarios (
    id              INTEGER       PRIMARY KEY AUTO_INCREMENT,
    horario_inicial VARCHAR(10)          NOT NULL,
    horario_final   VARCHAR(10)          NOT NULL,
    dias            VARCHAR (100) NOT NULL,
    super_id        INTEGER       NOT NULL,
    FOREIGN KEY (
        super_id
    )
    REFERENCES supermercado (id) 
);

CREATE TABLE provedores (
    id             INTEGER      PRIMARY KEY AUTO_INCREMENT,
    nombre_empresa VARCHAR (50) NOT NULL,
    super_id       INTEGER      NOT NULL,
    FOREIGN KEY (super_id)    REFERENCES supermercado (id) 
);

CREATE TABLE personal (
    id        INTEGER      PRIMARY KEY AUTO_INCREMENT,
    nombre    VARCHAR (50) NOT NULL,
    apellidos VARCHAR (50) NOT NULL,
    edad      INTEGER      NOT NULL,
    sexo      VARCHAR (50) NOT NULL,
    e_mail    VARCHAR (50) NOT NULL,
    telefono  INTEGER      NOT NULL,
    super_id  INTEGER      NOT NULL,
    FOREIGN KEY (super_id) REFERENCES supermercado (id) 
);

CREATE TABLE area_laboral (
    id          INTEGER      PRIMARY KEY AUTO_INCREMENT,
    nombre_area VARCHAR (50) NOT NULL,
    personal_id INTEGER      NOT NULL,
    FOREIGN KEY (personal_id) REFERENCES personal (id) 
);


CREATE TABLE clientes (
    id       INTEGER     PRIMARY KEY AUTO_INCREMENT,
    NIT      INTEGER     NOT NULL UNIQUE,
    nombre   VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    celular  INTEGER      NOT NULL,
    super_id INTEGER      NOT NULL,
    FOREIGN KEY (super_id) REFERENCES supermercado (id) 
);

CREATE TABLE venta (
    id          INTEGER     PRIMARY KEY AUTO_INCREMENT,
    nro_factura INTEGER     NOT NULL,
    importe     double NOT NULL,
    cliente_id  INTEGER NOT NULL,
    area_id     INTEGER NOT NULL,
    FOREIGN KEY (cliente_id) REFERENCES clientes (id),
    FOREIGN KEY (area_id) REFERENCES area_laboral (id) 
);

CREATE TABLE articulos (
    id              INTEGER      PRIMARY KEY AUTO_INCREMENT,
    codigo          INTEGER      NOT NULL,
    nombre_articulo VARCHAR (50) NOT NULL,
    precio          double NOT NULL,
    venta_id        INTEGER      NOT NULL,
    super_id        INTEGER      NOT NULL,
    cliente_id      INTEGER      NOT NULL,
    FOREIGN KEY (
        super_id
    )
    REFERENCES supermercado (id),
    FOREIGN KEY (
        cliente_id
    )
    REFERENCES clientes (id),
    FOREIGN KEY (
        venta_id
    )
    REFERENCES venta (id) 
);