
-- Autor: Ing. Jaime Zambrana Chacón
-- jameszambranachacon@gmail.com
CREATE TABLE clientes(
	id 			INTEGER PRIMARY KEY AUTO_INCREMENT,
	nombre 		VARCHAR(150),
	apellidos 	VARCHAR(150),
	telefono		VARCHAR(150)	
);
-- Insertar datos
INSERT INTO clientes(nombre,apellidos, telefono)
						   VALUES("juan","calvimonte","33554815"),
									("Romeo","Julian Montes","33553575"),
									("Roxana","Cespedes Cardenas","33599815");
-- 
