CREATE TABLE `ad_acciones`(
    `codigo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `descripcion` VARCHAR(255) NOT NULL,
    `abreviatura` VARCHAR(255) NOT NULL,
    `nombre` VARCHAR(255) NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `ad_aplicaciones`(
    `codigo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `descripcion` VARCHAR(255) NOT NULL,
    `abreviatura` VARCHAR(255) NOT NULL,
    `nombre` VARCHAR(255) NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `ad_logs`(
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `idad_usuario` INT NULL,
    `codad_accion` INT NULL,
    `fecha` DATETIME NOT NULL
);

CREATE TABLE `ad_opciones_usuarios`(
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `idad_usuario` INT NULL,
    `codad_opcion` INT NULL,
    `idad_logs` INT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `ad_usuarios`(
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `codad_aplicacion` INT NULL,
    `nombres` VARCHAR(255) NOT NULL,
    `apellidos` VARCHAR(255) NOT NULL,
    `nro_documento` VARCHAR(255) NOT NULL,
    `tipo_documento` VARCHAR(255) NOT NULL,
    `direccion` VARCHAR(255) NOT NULL,
    `tel_cel` VARCHAR(255) NOT NULL,
    `fecha_nacimiento` DATE NULL,
    `correo` VARCHAR(255) NOT NULL,
    `cargo` VARCHAR(255) NOT NULL,
    `login` VARCHAR(255) NOT NULL,
    `clave` VARCHAR(255) NOT NULL,
    `tipo_user` VARCHAR(255) NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `ad_modulos`(
    `codigo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `codad_aplicacion` INT NOT NULL,
    `descripcion` VARCHAR(255) NOT NULL,
    `abreviatura` VARCHAR(255) NOT NULL,
    `nombre` VARCHAR(255) NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `ad_opciones`(
    `codigo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `codad_modulo` INT NOT NULL,
    `codad_opcion` INT NULL,
    `opcion` VARCHAR(255) NOT NULL,
    `descripcion` VARCHAR(255) NOT NULL,
    `link` VARCHAR(255) NOT NULL,
    `nivel` INT NOT NULL,
    `orden` INT NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `ad_entidades`(
    `codigo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `denominacion` VARCHAR(255) NOT NULL,
    `descripcion_entidad` VARCHAR(255) NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

CREATE TABLE `cb_cuentas`(
    `codigo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `codcb_cuenta` INT NULL,
    `denominacion_cuenta` VARCHAR(255) NOT NULL,
    `descripcion_cuenta` VARCHAR(255) NOT NULL,
    `nivel` INT NOT NULL,
    `estado` VARCHAR(255) NOT NULL
);

ALTER TABLE
    `ad_logs` ADD CONSTRAINT `ad_logs_codad_accion_foreign` FOREIGN KEY(`codad_accion`) REFERENCES `ad_acciones`(`codigo`);
ALTER TABLE
    `ad_modulos` ADD CONSTRAINT `ad_modulos_codad_aplicacion_foreign` FOREIGN KEY(`codad_aplicacion`) REFERENCES `ad_aplicaciones`(`codigo`);
ALTER TABLE
    `ad_usuarios` ADD CONSTRAINT `ad_usuarios_codad_aplicacion_foreign` FOREIGN KEY(`codad_aplicacion`) REFERENCES `ad_aplicaciones`(`codigo`);
ALTER TABLE
    `ad_opciones_usuarios` ADD CONSTRAINT `ad_opciones_usuarios_idad_logs_foreign` FOREIGN KEY(`idad_logs`) REFERENCES `ad_logs`(`id`);
ALTER TABLE
    `ad_opciones_usuarios` ADD CONSTRAINT `ad_opciones_usuarios_idad_usuario_foreign` FOREIGN KEY(`idad_usuario`) REFERENCES `ad_usuarios`(`id`);
ALTER TABLE
    `ad_logs` ADD CONSTRAINT `ad_logs_idad_usuario_foreign` FOREIGN KEY(`idad_usuario`) REFERENCES `ad_usuarios`(`id`);
ALTER TABLE
    `ad_opciones` ADD CONSTRAINT `ad_opciones_codad_modulo_foreign` FOREIGN KEY(`codad_modulo`) REFERENCES `ad_modulos`(`codigo`);
ALTER TABLE
    `ad_opciones_usuarios` ADD CONSTRAINT `ad_opciones_usuarios_codad_opcion_foreign` FOREIGN KEY(`codad_opcion`) REFERENCES `ad_opciones`(`codigo`);